.. CHINO.io Python wrapper

This is the wrapper for the API of https://chino.io

More info: https://github.com/chinoio/chino-python

.. toctree::

    install
    docs

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
