.. CHINO.io Python wrapper

This is the wrapper for the API of https://chino.io

More info: https://github.com/chinoio/chino-python

INSTALL
=======

.. code-block:: python

    pip install chino

USAGE
------
.. code-block:: python

    from chino.api import ChinoAPIClient
    chino = ChinoAPIClient(<customer_id>, <customer_key>)
